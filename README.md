# Introduction

encypher.it is a project for GPG users that want to have a publicly available interface to let anonymous users send them a GPG encypted message.

You create your own account and define *inboxes*. Each inbox has its own link.
Everytime a user sends you a message, you get an email notification with the GPG encrypted text. You can also connect to the dashboard and review them.

A public instance with open registration is available at: https://encypher.it

# Deploy to docker

All container images are available on docker hub:

- https://cloud.docker.com/u/gmoigneu/repository/docker/gmoigneu/encypher_frontend
- https://cloud.docker.com/u/gmoigneu/repository/docker/gmoigneu/encypher_dash
- https://cloud.docker.com/u/gmoigneu/repository/docker/gmoigneu/encypher_dash-nginx

You can deploy the app to a Swarm cluster using the provided `docker-stack.yml` file.

Please change the required values:

```
APP_NAME: encypher.it
APP_ENV: production
APP_KEY: "base64:xxx"
APP_URL: "https://dash.encypher.it"
FRONTEND_URL: "https://encypher.it"
ENC_REGISTRATION: "true"
MAIL_DRIVER: smtp
MAIL_HOST: in-v3.mailjet.com
MAIL_PORT: 587
MAIL_USERNAME: null
MAIL_PASSWORD: null
MAIL_ENCRYPTION: tls
MAIL_FROM_NAME: "encypher.it"
MAIL_FROM_ADDRESS: noreply@encypher.it
```

Create a `docker config` called `encypher_frontend` with the following content:

```
{
    "dashUrl": "http://<url>/",
    "apiUrl": "http://<url>/api/"
}
```

Then create and deploy the stack. Once done, execute the following command in your `dash` container:

```
php artisan migrate
```

# Build images

```
docker build -f docker/dash-nginx/Dockerfile --pull --cache-from gmoigneu/encypher_dash-nginx:latest -t gmoigneu/encypher_dash-nginx:latest .
docker push gmoigneu/encypher_dash-nginx:latest
```

```
docker build -f docker/frontend/Dockerfile --pull --cache-from gmoigneu/encypher_frontend:latest -t gmoigneu/encypher_frontend:latest .
docker push gmoigneu/encypher_frontend:latest
```

```
docker build -f docker/dash-app/Dockerfile --pull --cache-from gmoigneu/encypher_dash:latest -t gmoigneu/encypher_dash:latest .
docker push gmoigneu/encypher_dash:latest
```