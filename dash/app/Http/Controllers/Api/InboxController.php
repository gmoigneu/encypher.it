<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Inbox;
use App\Message;
use App\Transformers\InboxTransformer;
use App\Transformers\MessageTransformer;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreMessage;
use App\Notifications\NewMessage;

class InboxController extends Controller
{
    public function show(Inbox $inbox)
    {
        return fractal($inbox, new InboxTransformer())->toArray();
    }

    public function message(Inbox $inbox, StoreMessage $request)
    {
        $message = Message::make([
            'message'  => $request->message,
            'sender'   => $request->sender,
        ]);

        $inbox->messages()->save($message);

        // Notify user
        $inbox->user->notify(new NewMessage($message));

        return fractal($message, new MessageTransformer())->toArray();
    }
}
