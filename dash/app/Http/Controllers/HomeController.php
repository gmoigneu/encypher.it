<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Message;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $messagesPerDay = Message::whereIn('inbox_id', Auth::user()->inboxes->pluck('id'))
            ->where('created_at', '>=', \Carbon\Carbon::today()->subDays(7))
            ->get()
            ->countBy(function ($item) {
                return $item->created_at->format('Y-m-d');
            });
        
        $lastWeek = collect();
        for($i = 0; $i < 7; $i++) {
            $lastWeek->put(\Carbon\Carbon::today()->subDays($i)->format('Y-m-d'), 0);
        }

        $messagesPerDay = $messagesPerDay->union($lastWeek)->sortKeys();

        return view('dashboard', [
            'inboxes'  => Auth::user()->inboxes()->with('messages')->get(),
            'messages' => Auth::user()->messages()->with('inbox')->limit(10)->orderBy('created_at', 'desc')->get(),
            'total_messages' => Auth::user()->messages()->count(),
            'messages_per_day' => $messagesPerDay
        ]);
    }
}
