<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Inbox;
use App\Http\Requests\StoreInbox;
use App\Http\Requests\UpdateInbox;

class InboxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(Inbox $i)
    {
        if ($i->user->id != Auth::user()->id) {
            abort(403);
        }

        return view('inbox', [
            'inbox' => $i
        ]);
    }

    public function create()
    {
        return view('inbox.create');
    }

    public function edit(Inbox $i)
    {
        return view('inbox.edit', [
            'inbox' => $i
        ]);
    }

    public function update(UpdateInbox $request, $id)
    {
        $inbox = Inbox::findOrFail($id);
        $inbox->update($request->validated());
        $inbox->save();
        return redirect()->route('inbox.show', ['i' => $inbox->id]);
    }

    public function store(StoreInbox $request)
    {
        $inbox = Inbox::make($validated = $request->validated());
        Auth::user()->inboxes()->save($inbox);
        return redirect()->route('inbox.show', ['i' => $inbox->id]);
    }
}
