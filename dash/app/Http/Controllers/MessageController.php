<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Message;
use App\Inbox;

class MessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(Inbox $i, Message $m)
    {
        if ($m->inbox->id != $i->id || $i->user->id != Auth::user()->id) {
            abort(403);
        }

        return view('message', [
            'inbox'    => $i,
            'message'  => $m
        ]);
    }
}
