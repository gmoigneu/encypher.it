<?php

namespace App\Http\Middleware;

use Closure;

class HasKey
{
    public function handle($request, Closure $next)
    {
        if(!$request->user()->pubkey) {
            return redirect()->route('profile.edit');
        }

        return $next($request);
    }
}
