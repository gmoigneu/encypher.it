<?php

namespace App;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sender',
        'message'
    ];

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public function inbox()
    {
        return $this->belongsTo('App\Inbox');
    }
}
