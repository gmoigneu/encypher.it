<?php

namespace App\Transformers;
use League\Fractal;
use App\Inbox;

class InboxTransformer extends Fractal\TransformerAbstract
{
	public function transform(Inbox $inbox)
	{
	    return [
	        'uuid'          => $inbox->uuid,
	        'email'         => $inbox->user->email,
            'name'          => $inbox->user->name,
            'fingerprint'   => $inbox->user->fingerprint,
            'pubkey'        => $inbox->user->pubkey,
            'links'   => [
                [
                    'rel' => 'self',
                    'uri' => '/inboxes/'.$inbox->uuid,
                ]
            ],
	    ];
	}
}