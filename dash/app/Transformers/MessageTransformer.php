<?php

namespace App\Transformers;
use League\Fractal;
use App\Message;

class MessageTransformer extends Fractal\TransformerAbstract
{
	public function transform(Message $message)
	{
	    return [
	        'id'      => $message->id,
	        'email'   => $message->inbox->user->email,
            'name'    => $message->inbox->user->name,
            'sender'  => $message->sender,
            'message' => $message->message,
            'links'   => [
                [
                    'rel' => 'self',
                    'uri' => '/messages/'.$message->id,
                ]
            ],
	    ];
	}
}