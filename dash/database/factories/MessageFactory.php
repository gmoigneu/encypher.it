<?php

use App\Inbox;
use App\Message;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Inbox::class, function (Faker $faker) {
    return [
        'name' => $faker->word()
    ];
});

$factory->define(Message::class, function (Faker $faker) {
    return [
        'sender' => $faker->email,
        'message' => "
        -----BEGIN PGP MESSAGE-----

        hQEMAwAAAAAAAAAAAQf/XD6cjodZ9CLuOoA04JuMIGn5yoz/BXHlt9ZRs/9fI49T
        CIWvSSLXpaviCNB8a9VUxXCsHtXdNhSM2JdzM6RKs2cW56Y7xQA+04WCPMgFLESJ
        3jw8Eqqwbo1QH+2Qwab4tecQJzNWUDfT2nzZ2z7hIKgMYjXjtrUT3y7gV+oS/273
        jRosbGahOPxpJVvk/U5iHFhVuNH6AAdR39A8wm5CEqF0z7QM3LL0gEhWvWfZMmRq
        KRiOcpd962N4soCWWtqxDcrX0Jzb3GCrRLXk9mV5A1RsPIVfugW6NdfGP+9t7AM8
        WvdOY4qDC2sfPK4p/FWH2YoYVnnjUR+lV2UDRm04XtLA0gG2Y4VmeCktMwcRIF09
        OVi8u56+GODaD+PZpa6b6ExZbLuQM1hyv/j6VOCd/BIdA8ZvgRRcvF9DSCqHB7RC
        4z2ayh16K1LQLiBVw+J+S/t8gNmU6x3r3aGWYHBNwQvJjRYUuTZP83m4vWcjOkAy
        DsdMx+6pVB+U++eP8tYv3Wbyv8/iqNDOeyKkInKP49peG/Jms3p80y/zSRv4S28+
        nF9fHrtNvGIbfOHmyBiEg77NDA+RXcYP1NGqZ5CQal7WukQNABOyrfJ4eJHV07qx
        Z7miaPujprJKJXjRr1PBYOMh79YHtAMztyjH+Rn0h61Otcu56t1YQ8buWPJ5IQnc
        ryo6n5paX12mDI0tKBb/gNRduK+r/fvaTIBCN5pJnegqLBqcXZZCIwHaBeW4XSj+
        c147vA4G0few+aR5UjyCAxt2+O4BLm1+LCkCg4CNW5V209oJiyKfojUfz1OCpNyg
        oVwc7Ii5B0fptcNtluAFndE4pwQM8+3ljiBx6cDP18Y9HbRGZvP2bnZ++ANeQviC
        lLWr1A==
        =zPbM
        -----END PGP MESSAGE-----
        "
    ];
});
