<?php

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'pubkey' => "
        -----BEGIN PGP PUBLIC KEY BLOCK-----

        mQENBFyvjxIBCADHuAYcFTDuCIq0MwXnUhu/hGDhwimFk2NcvZkQucVKL8mPDPkR
        onwX3RtY39Uoj+eOPoPntTuX7PCbzX0wvYdVpeKFE0gWnIQbvle2T3l6b16mgW2S
        aujJ8xn1BkR5Laqvj9IeKEnW31dSBVNdwEbBC6HJJJOZx1wLnWZI26MvpyX94b58
        vzoGVoP+VCZ2PX+6dmw8NrtvkQruBaYTfty/s8zRNrjo9TrP/expe3tEDt3bejfs
        Ohn8K7VlasTUpoLQEXFziKgkqfac7oNJXb7sDtmGmJRLAfkaUlHUGm+k0VDWwP5C
        znD6BL0DPvUbtnOL/PHL0NBARniNpd6xlKN5ABEBAAG0KUd1aWxsYXVtZSBNb2ln
        bmV1IDxndWlsbGF1bWVAbW9pZ25ldS5jb20+iQFOBBMBCAA4FiEEcJPkE8wlGpTV
        +kS1aWzzao8v9fYFAlyvjxICGwEFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AACgkQ
        aWzzao8v9fbaLggAx0bFBnCTWghORhkzzBfJm/p3cwYSLp5HMnyU6/d+j43Ia5Q+
        xyLpwSfWZSmulrhp7wTF1OoQ5dX18kY+p796xFZ63LgKrhpqAaKStNP4F9EJcjlH
        PLjzwMwWnQWcowUWI+0O78oqGjcCgXVk84/t+tHttXfqgxWKf1UFRACwOCACMaIf
        7mZ6rg50OYOPOLfRCOxST8LfpChaYL/Zv19WiOQg2jRCfT6JbFgnHVl8vBivfb/X
        r8z0VGxSdFfXq9HoVwztM/eMO/9strTHq285Cdzh2/2dP6oh1TAnBOa0PZOc7Xsq
        eWbIPekOrxuRx7hOMhNbLdowUHLqr0JAsA/YBbkBDQRcr49LAQgAvrAHeq4RWlZL
        OuIBIbRt+ligde+DGn5IS1AYav4nAN1eBuXCenB4moSwzg7Hp5QZ7xbQgbLICoLL
        wV6LYpmCk7LUCvLUdGn52qagqpnzDeOGb+otUC5CAPEkq1ZBarTZhxHFZXaBzLxk
        yIg0Zfd629d0OLGacLHPhaXRq/4wxoFWQzfKuCFf/j9dLwMG0eHPD3OV6iIcs2sJ
        ggEtiVrlybwmZiADXPxc2rLi/1h++tuV/ywJQwHElJO2yDMvGNw9EsN0f3QDjnon
        LBbWtpmpZY/ZSDp8VgPIyrBgq0qPeL5XUCFpS7+6jMA18Bua1xM+ntseLQd2DfQU
        cWwfDw0CswARAQABiQJrBBgBCAAgFiEEcJPkE8wlGpTV+kS1aWzzao8v9fYFAlyv
        j0sCGwIBPwkQaWzzao8v9fbAcyAEGQEIAB0WIQRFzCmILUTdC49KlTwj4QybeMZ6
        GQUCXK+PSwAKCRAj4QybeMZ6GUyIB/dh9pOEnd3QDJcn20FlrhB9ulP0AQblzbyB
        w1YBEQNVNhPbBhCgcIu54yejz8C58hr3tttD3TaMejpSZFdCwOARO1z/TvafkxQN
        28942zXXR9QtSqE953eYgZEAnsgxJi4VpdKofTM5UPrQIkw+BmVmW5Lvr7ywy6OB
        vyT8UO+WVzcfm83++w8Nhy21xyAerNq8O/AqXlyPwzJJSHqi2XpZrBYRSdt88fib
        Jzt6SSH7+gxuG61wNTUCSYLIFQedjht/AmcZYvRA/laCd3HYubc+QD1UQ6dFv+v8
        WB1Jd0RXonAoi1Nj/PFQQpMQCuUyGe/oLTZtwzfnhXME7j5lk9cYggf9GsxRSZu6
        KjVRUWVepOXZNQ8fw/pfe8WkM6tThyV31pHRVgRLqBJzQVhxCKb5SMcFS5dnfPCZ
        NDHzZpMnHWpyB9gKZhSwcyJ7gRyVpROl7B14s2PEtVGmG74gvA28+HhFiy+w17gn
        JVC+q7rpgMlK+urTt1Oko5xrXPSdeKGl+yrlFloYT8AQ9f+V8Qy2OWTIbLuVVt7/
        Uol4+CYa9BGGL4J8EhglxB//+sl5X9tEobDGCegLdKot7CKrT14j2swXo3q5TCFD
        WURIMUhwfhZi9F6JlVP2DDe8TqNgMDwJja4t555GPop2uPRSTBNPKIzQmhRLLrHn
        fuXly3ZBGRVktLkBDQRcr49oAQgAzmuM6QJudquxUN/K074HSUOESmcmTQQUKZeh
        4Jwo3quVfwoCUaqJ7F3ao9DWbK1iyI/kL5XouubtihSqY3sBC10v7h0qRa6fnTiz
        zqai5MrU0ObrkRQYoF0YoEVRzS+Spi9zoXHbxifemNfn0VoHX7L4b+tczg6pzsfy
        URx37N+szpPJ6Ni2cyfHv1ayYAaEfRqeICN+B0bH9rhl3gyvznADk801y6Cs9Wnc
        juwwfOnkbzA5BffzGtlfpaV6UjEBpCdW0Z4X3sl68BCFlut2NWFfycc+q3AvTkTq
        LCstYnY9fKSbmW2wUh11TO8d7TvbDpoXEwziGHSIA7gGnOQ2yQARAQABiQE2BBgB
        CAAgFiEEcJPkE8wlGpTV+kS1aWzzao8v9fYFAlyvj2gCGwwACgkQaWzzao8v9faB
        ngf9GbsGQlQa2jD0c/I7xR6oFElg84jiHx9GiSCOd58B62gsipCvjrtZ4bJbeuvE
        +8xQG9GgCvlY+JB31KWfsNXSHpiODqiJ56BGZkPQPtPsOzt719brpPfTNAq0nsRd
        ArnNTyqXRRiJP66dFvm6NJqfKVOBDIk6Tv/R62uaMKwYUo2zxScQzoveHEIJiUQR
        isVzHoSCPsRWBshPVQCubMVN0Xw0zFjJeYeUkOkA1wo3rP7axtXffKqcQqLUgPqV
        O4pzwvSa3WkT/VCj5OTrHE5376Lqk3AS2THlpQ9R1C48hER9b7vvyh3tHgoGpuwR
        ur4StGx+b0ghG3gMgev+hmrk2LkBDQRcr493AQgA032rRwJcMlIXegsKRnhPR7mJ
        ed9j4tXphVHtc0HuNeLQHgPECtlVLdE6+vKPYDunTKtQJaY/7PNJ17xSadHVQj1B
        rSjh1ZQd9Ij1vZmKQaFl+TL8Qga2rsq+7p8cq42T8eDiZApgBrikF64DMkKotqrH
        nOU8WzYXqZCRkloluWtnXqIY1xcX9btzTo+jb9vpKCEHdhD3JJ8q8mmmoJTspchT
        NDm9KXMc6M2eexRl3fBYutRQUCk+SbBI7Z1WVl+fbbbDzjqgKYS344xgtYnBH4NM
        7I6hMQDcbu3OcGuFKDJspgesRAxw6aP9jiCq3Wq1g9J3UemElygMAcxnYBQJjwAR
        AQABiQE2BBgBCAAgFiEEcJPkE8wlGpTV+kS1aWzzao8v9fYFAlyvj3cCGyAACgkQ
        aWzzao8v9fY+fQf/fUGfdT1wHzytKuFswGicxej7B8kmU+tzNbsio8HEALCTswIz
        v1px76N0Pu/FXU2/kBKpX/vYnMq7jdmD49DbFtYojlh/zPO44CbXtOHT5JC1kHfE
        JnpZVRnocl5EzzblfQPbewxFmSTqQsTB45/7koYCURTYpJ8jT6xAwY+Q8CVweS8L
        YRWLnqzkpIXpLKvVA0oW2g45jxMMdsLOs4w3XbBPAHZF7NM8SE8U+R0HPravAhrk
        gXM1rNjXv10mXMy5kKEyCaitWXe8Ehl1s2TJJSrgr+SSGfAf4GwRGRFswpvZnPZd
        lkqcaiacBEOwQj3vAeMpp6K4nQdMjDL5nrJNng==
        =K+VN
        -----END PGP PUBLIC KEY BLOCK-----
        ",
        'fingerprint' => "7093 E413 CC25 1A94 D5FA  44B5 696C F36A 8F2F F5F6"
    ];
});
