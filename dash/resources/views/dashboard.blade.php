@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')
    
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card bg-gradient-default shadow">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-light ls-1 mb-1">Overview</h6>
                                <h2 class="text-white mb-0">Messages per day</h2>
                            </div>
                            <div class="col">
                                <ul class="nav nav-pills justify-content-end">
                                    <li class="nav-item mr-2 mr-md-0" 
                                    data-toggle="chart"
                                    data-target="#chart-messages"
                                    data-update=""
                                    data-prefix=""
                                    data-suffix="">
                                        <a href="#" class="nav-link py-2 px-3 active" data-toggle="tab">
                                            <span class="d-none d-md-block">Day</span>
                                            <span class="d-md-none">D</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <!-- Chart -->
                        <div class="chart">
                            <!-- Chart wrapper -->
                            <canvas id="chart-messages" class="chart-canvas"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Inboxes</h3>
                            </div>
                            <div>
                                <a href="{{ route('inbox.create') }}" class="btn btn-sm btn-primary">Create a new inbox</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Link</th>
                                    <th scope="col">Messages</th>
                                    <th scope="col">Last message</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($inboxes as $inbox)
                                <tr>
                                    <th scope="row">
                                        {{ $inbox->name }}
                                    </th>
                                    <td>
                                        <a href="{{ config('app.frontend_url').'/inbox/'.$inbox->id }}">{{ $inbox->id }}</a>
                                    </td>
                                    <td>
                                        {{ $inbox->messages->count() }}
                                    </td>
                                    <td>
                                        @if($inbox->messages()->count())
                                            {{ $inbox->messages()->orderBy('created_at', 'desc')->first()->created_at }}
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('inbox.show', ['inbox' => $inbox->id]) }}" class="btn btn-icon btn-3 btn-default btn-sm">
                                            <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                            <span class="btn-inner--text">View</span>
                                        </a>
                                        <a href="{{ route('inbox.edit', ['inbox' => $inbox->id]) }}" class="btn btn-icon btn-3 btn-secondary btn-sm">
                                            <span class="btn-inner--text">Edit</span>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Last messages</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Sender</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Inbox</th>
                                    <th scope="col">Actions</th>
                                </tr>
                    
                            </thead>
                            <tbody>
                                @foreach ($messages as $message)
                                <tr>
                                    <th scope="row">{{ $message->sender }}</th>
                                    <td>{{ $message->created_at }}</td>
                                    <td>{{ $message->inbox->name }}</td>
                                    <td>
                                        <a href="{{ route('message.show', ['inbox' => $message->inbox->id, 'message' => $message->id]) }}" class="btn btn-icon btn-3 btn-default btn-sm">
                                            <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                            <span class="btn-inner--text">View</span>
                                        </a>
                                        <!--<a href="{{ route('message.destroy', ['inbox' => $message->inbox->id, 'message' => $message->id]) }}">Delete</a>-->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script>
                messagesData = {!! json_encode($messages_per_day->values()->all()) !!}
                messagesKeys = {!! json_encode($messages_per_day->keys()->all()) !!}
        </script>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>

    
@endpush