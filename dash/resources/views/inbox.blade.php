@extends('layouts.app')

@section('content')
    @include('users.partials.header', [
        'title' => __('Inbox') . ' '. $inbox->name,
        'description' => __('ID') . ': ' . $inbox->id,
        'class' => 'col-md-12'
    ])  


    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card bg-secondary shadow">
                    <div class="card-body">
                        <h6 class="heading-small text-muted mb-4">{{ __('Meta information') }}</h6>

                        <div class="pl-lg-4">
                            <div class="form-group">
                                <p class="form-control-label">{{ __('Public link') }}: <a href="{{ config('app.frontend_url').'/inbox/'.$inbox->id }}">{{ config('app.frontend_url').'/inbox/'.$inbox->id }}</a></p>
                            </div>

                            <a href="{{ route('inbox.edit', ['inbox' => $inbox->id]) }}" class="btn btn-icon btn-3 btn-default">
                                <span class="btn-inner--text">Edit inbox</span>
                            </a>
                        </div>

                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Messages ({{ $inbox->messages()->count() }})</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Sender</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Actions</th>
                                </tr>
                    
                            </thead>
                            <tbody>
                                @foreach ($inbox->messages()->orderBy('created_at', 'desc')->get() as $message)
                                    <tr>
                                        <th scope="row"><a href="{{ route('message.show', ['inbox' => $inbox->id, 'message' => $message->id]) }}">{{ $message->sender }}</a></th>
                                        <td>{{ $message->created_at }}</td>
                                        <td>
                                            <a href="{{ route('message.show', ['inbox' => $inbox->id, 'message' => $message->id]) }}" class="btn btn-icon btn-3 btn-default btn-sm">
                                                <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                <span class="btn-inner--text">View</span>
                                            </a>
                                            <!--<button href="{{ route('message.destroy', ['inbox' => $inbox->id, 'message' => $message->id]) }}" class="btn btn-icon btn-3 btn-secondary btn-sm" type="button">
                                                <span class="btn-inner--icon"><i class="ni ni-fat-remove"></i></span>
                                            </button>-->
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection