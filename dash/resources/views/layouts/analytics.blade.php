<script type='text/javascript'>
    //some default pre init
    var Countly = Countly || {};
    Countly.q = Countly.q || [];

    //provide countly initialization parameters
    Countly.app_key = '{{ config("app.analytics_key") }}';
    Countly.url = '{{ config("app.analytics_server") }}';

    Countly.q.push(['track_sessions']);
    Countly.q.push(['track_pageview']);
    Countly.q.push(['track_clicks']);
    Countly.q.push(['track_errors']);
    Countly.q.push(['track_links']);
    Countly.q.push(['track_forms']);

    //load countly script asynchronously
    (function() {
        var cly = document.createElement('script'); cly.type = 'text/javascript';
        cly.async = true;
        //enter url of script here
        cly.src = '{{ config("app.analytics_server") }}/sdk/web/countly.min.js';
        cly.onload = function(){Countly.init()};
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(cly, s);
    })();
</script>
<noscript><img src='{{ config("app.analytics_server") }}/pixel.png?app_key={{ config("analytics_key") }}&begin_session=1'/></noscript>