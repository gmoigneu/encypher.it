@extends('layouts.app', ['title' => __('View message')])

@section('content')
    @include('users.partials.header', [
        'title' => __('From') . ' '. $message->sender,
        'description' => __('Message') . ': ' . $message->id,
        'class' => 'col-md-12'
    ])  


    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-8 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-body">
                        <h6 class="heading-small text-muted mb-4">{{ __('Meta information') }}</h6>

                        <div class="pl-lg-4">
                            <div class="form-group">
                                <label class="form-control-label" for="input-created_at">{{ __('Sent at') }}</label>
                                <input type="created_at" disabled name="created_at" id="input-created_at" class="form-control form-control-alternative" placeholder="{{ __('Fingerprint') }}" value="{{ $message->created_at }}" required>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="input-fingerprint">{{ __('Fingerprint') }}</label>
                                <input type="fingerprint" disabled name="fingerprint" id="input-fingerprint" class="form-control form-control-alternative" placeholder="{{ __('Fingerprint') }}" value="{{ $inbox->user->fingerprint }}" required>
                            </div>

                            <div class="form-group">
                                <p class="form-control-label" for="input-fingerprint">{{ __('Inbox') }}: <a href="{{ route('inbox.show', ['inbox' => $inbox->id]) }}">{{ $inbox->name }}</a></p>
                            </div>
                        </div>

                        <h6 class="heading-small text-muted mb-4">{{ __('Content') }}</h6>

                        <div class="pl-lg-4">
                            <div class="form-group">
                                <label class="form-control-label" for="input-pubkey">{{ __('Encrypted content') }}</label>
                                <textarea name="content" id="content" style="width: 100%" rows="30">{{ $message->message }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection