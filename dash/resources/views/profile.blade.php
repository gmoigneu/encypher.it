@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mb-4 mt-4">
        <div class="col-md-12">
            <h1>Edit profile</h1>
        </div>
    </div>
    @if(!$user->pubkey)
    <div class="alert alert-danger">Your profile does not have any public key. Please upload one to begin using our tool!</div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <div class="row mb-4 mt-4">
        <div class="col-md-12">
        <form method="POST" action="{{ route('profile.update') }}">
            @method('PUT')
            @csrf
                <div class="card">
                    <div class="card-header">Informations</div>

                    <div class="card-body">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Your email address" value="{{ $user->email }}">
                            <small id="emailHelp" class="form-text text-muted">This will get verified.</small>
                        </div>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp" placeholder="Your display name" value="{{ $user->name }}">
                            <small id="nameHelp" class="form-text text-muted">This will be shown to senders.</small>
                        </div>
                        <div class="form-group">
                                <label for="name">Key fingerprint</label>
                                <input type="text" class="form-control" name="fingerprint" id="name" aria-describedby="nameHelp" placeholder="Your key fingerprint" value="{{ $user->fingerprint }}">
                            </div>
                        <div class="form-group">
                            <label for="pubkey">GPG public key</label>
                            <textarea rows="10" style="width:100%" class="form-control" name="pubkey" id="pubkey" aria-describedby="pubkeyHelp" placeholder="Your public key">{{ $user->pubkey }}</textarea>
                            <small id="pubkeyHelp" class="form-text text-muted">gpg --armor --export $KEYID</small>
                        </div>
                        <button type="submit" class="btn btn-primary">Update profile</button>
                    </div>
                </div>
        </form>
        </div>
    </div>
</div>
@endsection