<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::middleware(['verified', 'auth'])->group(function () {
    Route::middleware(['haskey'])->group(function () {
        Route::get('/', 'HomeController@index')->name('home');
        Route::resource('/i', 'InboxController')->names([
            'show' => 'inbox.show',
            'update' => 'inbox.update',
            'store' => 'inbox.store',
            'destroy' => 'inbox.destroy',
            'edit' => 'inbox.edit',
            'create' => 'inbox.create'
        ])->except(['index']);
        Route::resource('/i/{i}/m', 'MessageController')->names([
            'show'    => 'message.show',
            'destroy' => 'message.destroy'
        ])->only(['show', 'destroy']);
    });
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});



