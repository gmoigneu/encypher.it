import pkg from './package'
require('dotenv').config()
 

export default {
  mode: 'spa',
  
  env: {
    dashUrl: process.env.DASH_URL || 'http://localhost:8095/'
  },
  /*
   ** Headers of the page
   */
  head: {
    title: "encypher.it",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Quicksand' },
      { rel: 'stylesheet', href: "https://use.fontawesome.com/releases/v5.8.1/css/all.css", integrity: "sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf", crossorigin:"anonymous" }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },

  /*
   ** Global CSS
   */
  css: [
    '~/assets/css/tailwind.css',
    '~/assets/css/encypher.css'
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    'nuxt-client-init-module',
    '@marcdiethelm/nuxtjs-countly'
  ],
  /*
   ** Axios module configuration
   */
  axios: {
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options : {
            fix : true
          }
        })
      }
    }
  },

  countly: {
    url: process.env.ANALYTICS_SERVER,
    app_key: process.env.ANALYTICS_KEY,
    trackerSrc: process.env.ANALYTICS_JS,
    trackers: [
      'track_sessions',
      'track_pageview',
      'track_links'
    ],
    debug: process.env.NODE_ENV !== 'production'
  }
}
