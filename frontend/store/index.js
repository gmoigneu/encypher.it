import axios from 'axios'

export const state = () => ({
  dashUrl: 'http://localhost:8095',
  apiUrl: 'http://localhost:8095/api'
})

export const actions = {
  async nuxtClientInit({ commit }, context) {
    const { data } = await axios.get('/config.json')
    commit('set', data)
  }
}

export const mutations = {
  set(state, config) {
    state.dashUrl = config.dashUrl
    state.apiUrl = config.apiUrl
  }
}
